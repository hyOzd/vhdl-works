--
--	VGA Test image generator
--
--	by Hasan Yavuz ÖZDERYA ( hy [at] ozderya.net )
--
--	KTU DSPLAB , 12 Nov 2014
--
--	This block generates an interpolation of RGB colors in one axis

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library std;
use std.textio.all;

entity vga_test_image is

    Generic(n     : integer := 8;
            h_res : integer := 640;
            v_res : integer := 480;
            refresh_rate : integer := 60;
            animation_seconds : integer := 5); -- NOT PRECISE AT ALL

    Port(cx, cy      : in std_logic_vector(15 downto 0);
         red,
         green,
         blue : out std_logic_vector(n-1 downto 0);
         frame_start : in std_logic;
         clock       : in std_logic;
         reset       : in std_logic);

end entity;

architecture rtl of vga_test_image is

    --type array_type is array(natural range<>) of std_logic_vector(n-1 downto 0);
    --subtype the_type is std_logic_vector(n-1 downto 0);

    constant ANIM_LENGTH : integer := animation_seconds*refresh_rate;
    constant x_step : integer := h_res / ANIM_LENGTH;
    signal x_count : integer range 0 to (h_res + x_step);

begin

    -- uses frame_start as its clock
    counter: process(frame_start, reset)
        variable count : integer range 0 to (h_res + x_step);
    begin

        if reset = '1' then
            count := h_res;
        elsif rising_edge(frame_start) then
            if count >= h_res then
               count := 0;
            else
                count := count + x_step;
            end if;
        end if;

        x_count <= count;

    end process; -- offset counter

    process(clock, reset)
        variable x : integer range 0 to 2*h_res;
        variable y : integer range 0 to v_res;

        -- 65536 is picked because dividing is possible with shifting
        constant norm_d: integer := 65536;
        constant norm : integer := 255*norm_d/h_res;
        variable ratio : integer range 0 to 255;

        variable cred, cgreen, cblue : integer range 0 to 255;

        variable l : line;
    begin

        if reset = '1' then
            red <= (others => '0');
            green <= (others => '0');
            blue <= (others => '0');
            x := 0;
            ratio := 0;
        elsif rising_edge(clock) then

            x := x_count + to_integer(unsigned(cx));
            if x > h_res then
                x := x-h_res;
            end if;
            y := to_integer(unsigned(cy));

            ratio := (x*norm)/norm_d;


            if ratio < 255 / 2 then
                cblue := 255 - 2*ratio;
            else
                cblue := 0;
            end if;

            if ratio > 255 / 2 then
                cred := 2*ratio - 255;
            else
                cred := 0;
            end if;

            --write(l, cblue);
            --write(l, ',');
            --write(l, cgreen);
            --writeline(output, l);

            cgreen := 255 - cblue - cred;

            red <= std_logic_vector(to_unsigned(cred, n));
            blue <= std_logic_vector(to_unsigned(cblue, n));
            green <= std_logic_vector(to_unsigned(cgreen, n));

        end if;

    end process;

end architecture rtl;

architecture rows_rgb of vga_test_image is
    signal y : integer range 0 to v_res := 0;
begin

    y <= to_integer(unsigned(cy));

    red <= (others => '1') when y <= v_res/3 else
           (others => '0');

    green <= (others => '1') when y > v_res/3 and y < 2*v_res/3 else
           (others => '0');

    blue <= (others => '1') when y >= 2*v_res/3 else
           (others => '0');

end architecture rows_rgb;

architecture light_spectra of vga_test_image is

    type rgb8 is record
        red : integer range 0 to 255;
        green : integer range 0 to 255;
        blue : integer range 0 to 255;
    end record rgb8;

    -- converts wavelength to RGB colors
    function wave2rgb(wavelength:in integer) return rgb8 is
        variable red, green, blue : real;
        variable factor : real;
        variable r_wavelength : real;
        variable rgb_color : rgb8;
    begin

        r_wavelength := real(wavelength);

        if wavelength < 380 then
            red := 0.0;
            green := 0.0;
            blue := 0.0;
        elsif wavelength < 440 then
            red := - (r_wavelength - 440.0) / (440.0 - 380.0);
            green := 0.0;
            blue := 1.0;
        elsif wavelength < 490 then
            red := 0.0;
            green := (r_wavelength - 440.0) / (490.0 - 440.0);
            blue := 1.0;
        elsif wavelength < 510 then
            red := 0.0;
            green := 1.0;
            blue := -(r_wavelength - 510.0) / (510.0 - 490.0);
        elsif wavelength < 580 then
            red := (r_wavelength - 510.0) / (580.0 - 510.0);
            green := 1.0;
            blue := 0.0;
        elsif wavelength < 645 then
            red := 1.0;
            green := -(r_wavelength - 645.0) / (645.0 - 580.0);
            blue := 0.0;
        elsif wavelength <= 780 then
            red := 1.0;
            green := 0.0;
            blue := 0.0;
        else
            red := 0.0;
            green := 0.0;
            blue := 0.0;
        end if;

        -- let the intensity fall of near the vision limits
        if wavelength < 380 then
            factor := 0.0;
        elsif wavelength < 420 then
            factor := 0.3 + 0.7 * (real(wavelength) - 380.0) / (420.0 - 380.0);
        elsif wavelength < 700 then
            factor  := 1.0;
        elsif wavelength < 780 then
            factor := 0.3 + 0.7 * (780.0 - real(wavelength)) / (780.0 - 700.0);
        else
            factor := 0.0;
        end if;

        red := red * factor * 255.0;
        green := green * factor * 255.0;
        blue := blue * factor * 255.0;

        rgb_color.red := integer(red);
        rgb_color.green := integer(green);
        rgb_color.blue := integer(blue);

        return rgb_color;

    end function wave2rgb;

    constant spectrum_start : real := 380.0;
    constant spectrum_end : real := 780.0;

    -- BUG: nvc fails to initialize an array of record https://github.com/nickg/nvc/issues/95
    --type rgb8_array is array (0 to h_res-1) of rgb8;
    --signal spectra : rgb8_array;

    -- this array will contain RGB values for each horizontal pixel
    type color_array is array (0 to h_res-1) of std_logic_vector(n-1 downto 0);
    signal spectra_red, spectra_green, spectra_blue : color_array;

    constant ANIM_LENGTH : integer := animation_seconds*refresh_rate;
    constant x_step : integer := h_res / ANIM_LENGTH;
    signal x_count : integer range 0 to (h_res + x_step);

begin

    -- create the spectrum color values for each horizontal pixel
    gen_spectra:for i in 0 to h_res-1 generate
        constant wavelength : integer :=
            integer(( real(i) / real(h_res)) *
                    (spectrum_end - spectrum_start) + spectrum_start);
        constant val : rgb8 := wave2rgb(wavelength);
    begin
        --spectra(i) <= val;
        spectra_red(i) <= std_logic_vector(to_unsigned(val.red, n));
        spectra_green(i) <= std_logic_vector(to_unsigned(val.green, n));
        spectra_blue(i) <= std_logic_vector(to_unsigned(val.blue, n));
    end generate;

    -- this is the counter used to shift the image horizontally
    -- uses frame_start as its clock
    counter: process(frame_start, reset)
        variable count : integer range 0 to (h_res + x_step);
    begin

        if reset = '1' then
            count := h_res;
        elsif rising_edge(frame_start) then

            if count >= h_res then
                count := 0;
            else
                count := count + x_step;
            end if;

            x_count <= count;

        end if;
    end process; -- offset counter

    -- output driving process
    process(clock, reset)
        variable x : integer range 0 to 2*h_res;
    begin

        if reset = '1' then

            red <= (others => '0');
            green <= (others => '0');
            blue <= (others => '0');
            x := 0;

        elsif rising_edge(clock) then
            x := x_count + to_integer(unsigned(cx));
            if x > h_res-1 then
                x := x-h_res;
            end if;

            --red <= std_logic_vector(to_unsigned(spectra(x).red, n));
            --green <= std_logic_vector(to_unsigned(spectra(x).green, n));
            --blue <= std_logic_vector(to_unsigned(spectra(x).blue, n));

            red <= spectra_red(x);
            green <= spectra_green(x);
            blue <= spectra_blue(x);

        end if;

    end process;

end architecture light_spectra;
