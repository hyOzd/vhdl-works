library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_vga_test_image is
end entity;

architecture behav of tb_vga_test_image is

    signal clk:std_logic;
    signal reset:std_logic;
    signal stop_clock:boolean := false;
    constant CLK_PERIOD:time := 10 ns;

    constant n : integer := 8;

    signal cx, cy : std_logic_vector(15 downto 0);
    signal red, green, blue : std_logic_vector(n-1 downto 0);
    signal frame_start : std_logic;

begin

    uut:entity work.vga_test_image(light_spectra)
        port map(
            cx, cy,
            red, green, blue,
            frame_start,
            clk,
            reset
        );

    clkproc:process
    begin
        if (not stop_clock) then
            clk <= '1';
            wait for CLK_PERIOD/2;
            clk <= '0';
            wait for CLK_PERIOD/2;
        else
            wait;
        end if;
    end process clkproc;

    stimproc:process
    begin
        reset <= '1';
        frame_start <= '0';
        wait for CLK_PERIOD;
        reset <= '0';

        for i in 0 to 639 loop
            cx <= std_logic_vector(to_unsigned(i, 16));
            wait for CLK_PERIOD;
        end loop;

        stop_clock <= true;
        wait;
    end process stimproc;

end behav;
