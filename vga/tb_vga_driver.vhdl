library ieee;
use ieee.std_logic_1164.all;

entity tb_vga_driver is
end entity;

architecture behav of tb_vga_driver is

    signal clk:std_logic;
    signal reset:std_logic;
    signal stop_clock:boolean := false;
    constant CLK_PERIOD:time := 1.0 us / 25.175; -- 25.175 MHz

    constant n : integer := 8;

    -- data inputs
    signal d_red, d_green, d_blue : std_logic_vector(n-1 downto 0)
        := (others => '0');

    signal red, green, blue : std_logic_vector(n-1 downto 0);

    signal h_sync, v_sync : std_logic;

    signal row_start, frame_start : std_logic;

    signal cx, cy : std_logic_vector(15 downto 0);

begin

    uut:entity work.vga_driver
        --generic map()
        port map(
            d_red, d_green, d_blue,
            red, green, blue,
            h_sync, v_sync, open, open,
            frame_start, row_start,
            cx, cy,
            clk,
            reset
        );

    test_image:entity work.vga_test_image
        port map(
            cx, cy,
            d_red, d_green, d_blue,
            frame_start,
            clk,
            reset
        );

    clkproc:process
    begin
        if (not stop_clock) then
            clk <= '1';
            wait for CLK_PERIOD/2;
            clk <= '0';
            wait for CLK_PERIOD/2;
        else
            wait;
        end if;
    end process clkproc;

    stimproc:process
    begin
        reset <= '1';
        wait for CLK_PERIOD*1.5;
        reset <= '0';

        wait for CLK_PERIOD * 800 * 800;
        --wait for CLK_PERIOD * 50;

        stop_clock <= true;
        wait;
    end process stimproc;

end behav;
