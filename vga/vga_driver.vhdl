-- VGA Driver module
--
--	by Hasan Yavuz ÖZDERYA ( hy [at] ozderya.net )
--
--	KTU DSPLAB , 11 Nov 2014
--
-- Designed to drive ADV7123. This block doesn't drive the data inputs. Another
-- block (source) is required for this.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use ieee.math_real.all;

entity vga_driver is

    Generic(n : integer := 8;

            -- these defaults are for mode: 640x480@60Hz
            h_res : integer := 640;
            v_res : integer := 480;

            -- pixel clock
            h_front_porch : integer := 16;
            h_sync_pulse : integer := 96;
            h_back_porch : integer := 48;

            -- row (800 pixel clocks)
            v_front_porch : integer := 10;
            v_sync_pulse : integer := 2;
            v_back_porch : integer := 33;

            -- true: positive, false: negative polarity
            h_sync_pol : boolean := False;
            v_sync_pol : boolean := False
            );

    Port(d_red,
         d_green,
         d_blue : in std_logic_vector(n-1 downto 0);

         red,
         green,
         blue : out std_logic_vector(n-1 downto 0);

         h_sync,
         v_sync,
         nBlank,
         nSync: out std_logic;

         frame_start,
         row_start : out std_logic;

         cx,
         cy : out std_logic_vector(15 downto 0);

         clock	: in	std_logic; -- pixel clock
         reset	: in	std_logic
         );

end entity;

architecture rtl of vga_driver is

    --type array_type is array(natural range<>) of std_logic_vector(n-1 downto 0);
    --subtype the_type is std_logic_vector(n-1 downto 0);

    --signal column_count : unsigned(10 downto 0);
    --signal row_count : unsigned(10 downto 0);

    signal blank : std_logic;
    signal s_h_sync : std_logic;
    signal s_v_sync : std_logic;

begin

    nBlank <= '0';
    nSync <= '1';

    h_sync <= s_h_sync when h_sync_pol else
              not s_h_sync;
    v_sync <= s_v_sync when v_sync_pol else
              not s_v_sync;

    red <= d_red when blank = '0' else
           (others => '0');
    green <= d_green when blank = '0' else
           (others => '0');
    blue <= d_blue when blank = '0' else
           (others => '0');

    process(clock, reset)
        constant max_column : integer :=
            h_res + h_front_porch + h_sync_pulse + h_back_porch - 1;
        constant max_row : integer :=
            v_res + v_front_porch + v_sync_pulse + v_back_porch - 1;

        variable column_count : integer range 0 to max_column+1 := 0;
        variable row_count : integer range 0 to max_row+1 := 0;

        variable is_blank : boolean;
    begin

        if reset = '1' then

            column_count := max_column;
            row_count := max_row;
            is_blank := false;
            blank <= '1';
            s_h_sync <= '0';
            s_v_sync <= '0';
            row_start <= '0';
            frame_start <= '0';
            cx <= std_logic_vector(to_unsigned(0, 16));
            cy <= std_logic_vector(to_unsigned(0, 16));

        elsif rising_edge(clock) then

            -- increase column and row counter
            column_count := column_count + 1;

            if column_count = max_column+1 then

                column_count := 0;
                row_count := row_count + 1;

                if row_count = max_row+1 then
                    row_count := 0;
                end if;
            end if;

            -- h_sync output
            if column_count < h_res then
                s_h_sync <= '0';
            elsif column_count < h_res + h_front_porch then
                s_h_sync <= '0';
            elsif column_count < h_res + h_front_porch + h_sync_pulse then
                s_h_sync <= '1';
            else
                s_h_sync <= '0';
            end if;

            -- v_sync output
            if row_count < v_res then
                s_v_sync <= '0';
            elsif row_count < v_res + v_front_porch then
                s_v_sync <= '0';
            elsif row_count < v_res + v_front_porch + v_sync_pulse then
                s_v_sync <= '1';
            else
                s_v_sync <= '0';
            end if;

            -- blank status
            if row_count >= v_res or column_count >= h_res then
                is_blank := true;
            else
                is_blank := false;
            end if;

            -- frame signals
            if column_count = max_column then
                row_start <= '1';
                if (row_count = max_row) then
                    frame_start <= '1';
                end if;
            else
                row_start <= '0';
                frame_start <= '0';
            end if;

            -- coordinate outputs
            if column_count < h_res and not is_blank then
                cx <= std_logic_vector(to_unsigned(column_count, 16));
            else
                cx <= std_logic_vector(to_unsigned(h_res, 16));
            end if;

            if row_count < v_res then
                cy <= std_logic_vector(to_unsigned(row_count, 16));
            else
                cy <= std_logic_vector(to_unsigned(v_res, 16));
            end if;

            -- blank signal
            if is_blank then
                blank <= '1';
            else
                blank <= '0';
            end if;

        end if;

    end process;

end architecture rtl;
