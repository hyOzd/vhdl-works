--
--	Linear feedback shift register for Random Bit seq. needs
--	
--	by Hasan Yavuz �ZDERYA ( hy [at] ozderya.net ) 
--	Trabzon, KTU DSPLAB 15.05.2013
--	
--	polynoms are taken from http://www.xilinx.com/support/documentation/application_notes/xapp052.pdf

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lfsr is
	
	Generic( n		: integer := 16;
			 seed	: integer := 1
			);
	
	Port( wout	: out 	std_logic_vector(n-1 downto 0);
		  bout	: out	std_logic;
		  clk	: in	std_logic;
		  reset	: in	std_logic
		 );
		  	
end entity lfsr;

architecture rtl of lfsr is
	signal reg : std_logic_vector(n-1 downto 0) := std_logic_vector(to_unsigned(seed,n));
begin
	
	wout <= reg;
		
	process(clk,reset)
	begin
		
		if reset = '1' then
			
			reg <= std_logic_vector(to_unsigned(seed,n));
			bout <= '0';
						
		elsif rising_edge(clk) then
			
			case n is
				when 2 =>
					reg <= reg(n-2 downto 0) & (reg(1) xor '1');
				when 3 => 
					reg <= reg(n-2 downto 0) & (reg(2) xor reg(1));
				when 4 =>	
					reg <= reg(n-2 downto 0) & (reg(3) xor reg(2));
				when 5 =>
					reg <= reg(n-2 downto 0) & (reg(4) xor reg(2));
				when 6 =>
					reg <= reg(n-2 downto 0) & (reg(5) xor reg(4));
				when 7 =>
					reg <= reg(n-2 downto 0) & (reg(6) xor reg(5));
				when 8 =>
					reg <= reg(n-2 downto 0) & (reg(7) xor reg(5) xor reg(4) xor reg(3));
				when 9 =>
					reg <= reg(n-2 downto 0) & (reg(8) xor reg(4));
				when 10 =>
					reg <= reg(n-2 downto 0) & (reg(9) xor reg(6));
				when 11 =>
					reg <= reg(n-2 downto 0) & (reg(10) xor reg(8));
				when 12 =>
					reg <= reg(n-2 downto 0) & (reg(11) xor reg(5) xor reg(3) xor reg(0));
				when 13 =>
					reg <= reg(n-2 downto 0) & (reg(12) xor reg(3) xor reg(2) xor reg(0));
				when 14 =>
					reg <= reg(n-2 downto 0) & (reg(13) xor reg(4) xor reg(2) xor reg(0));
				when 15 =>
					reg <= reg(n-2 downto 0) & (reg(14) xor reg(13));
				when 16 =>
					reg <= reg(n-2 downto 0) & (reg(15) xor reg(14) xor reg(12) xor reg(3));
				when others =>
					reg <= (others => '0');
			end case;
			
			bout <= reg(reg'LEFT);
			
		end if;
		
	end process;
	
end architecture;