library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;

entity tb_lfsr is
end entity;

architecture behav of tb_lfsr is
	
	signal clk:std_logic;
	signal stop_clock:boolean := false;
	constant CLK_PERIOD:time := 10 ns;
	
	constant n :integer := 16;
	signal wout : std_logic_vector(n-1 downto 0);
	signal bout : std_logic;
	signal reset : std_logic;
	
begin

	uut:entity work.lfsr
		generic map(n)
		port map(
			wout,
			bout,
			clk,
			reset
		);

	clkproc:process
	begin
		if (not stop_clock) then
			clk <= '0';
			wait for CLK_PERIOD/2;
			clk <= '1';
			wait for CLK_PERIOD/2;
		else
			wait;
		end if;
	end process clkproc;
	
	stimproc:process
	begin
		reset <= '1';
		wait for CLK_PERIOD;
		reset <= '0';
		
		wait for CLK_PERIOD*(2**n);
		wait for CLK_PERIOD*5;
		
		stop_clock <= true;
		wait;
	end process stimproc;

end behav;