-- 
--	Register Controller module with Read and Write support
--
--	by Hasan Yavuz �ZDERYA ( hy [at] ozderya.net )
--
--	Trabzon KTU DSPLAB, 18.05.2013
--

library ieee;
use ieee.std_logic_1164.all;

package regcontroller_types is
	constant n : integer := 16;
	subtype reg is std_logic_vector(n-1 downto 0);
	type regs is array (natural range<>) of reg;
	subtype addr_type is std_logic_vector(7 downto 0);
end package;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.regcontroller_types.all;

entity regcontroller is
	
	Generic(--n		: integer := 16;	-- bus width !! controlled from package sorry!
										-- no tool allows package generics for the moment :(
			NRout	: integer := 4;		-- number of output registers
			Nrin	: integer := 4		-- number of input registers
			);
	
	Port(regouts	: out	regs(0 to NRout-1);
		 regins		: in	regs(0 to NRin-1);
		 addr		: in	addr_type;
		 datain		: in	reg;
		 dataout	: out	reg;
		 r_en,w_en	: in	std_logic;
		 clk,reset	: in	std_logic
		 );
	
end entity;

architecture rtl of regcontroller is

	-- ram signals
	signal ram_ro,ram_ri	: regs(0 to NRout-1);

begin
	
	
	process(clk,reset)
	begin
		
		if reset = '1' then
			ram_ro <= (others => (others => '0'));
			ram_ri <= (others => (others => '0'));
			dataout <= (others => '0');
		elsif rising_edge(clk) then
			
			regouts <= ram_ro;
			ram_ri <= regins;
			
			if w_en = '1' then
				
				ram_ro(to_integer(unsigned(addr))) <= datain;
				
			elsif r_en = '1' then
				
				dataout <= ram_ri(to_integer(unsigned(addr)));
				
			end if;
			
		end if;
		
	end process;

end architecture rtl;