library ieee;
use ieee.std_logic_1164.all;

library work;
use work.regcontroller_types.all;

entity tb_regcontroller is
end entity;

architecture behav of tb_regcontroller is
	
	signal clk:std_logic;
	signal reset:std_logic;
	signal stop_clock:boolean := false;
	constant CLK_PERIOD:time := 10 ns;
	
	signal regouts,regins : regs(0 to 3) := (others => (others => '0'));
	signal addr: addr_type;
	signal datain,dataout : reg;
	signal r_en,w_en : std_logic;
	
begin

	uut:entity work.regcontroller
		generic map(4,4)
		port map(
			regouts,
			regins,
			addr,
			datain,dataout,
			r_en,w_en,
			clk,
			reset
		);

	clkproc:process
	begin
		if (not stop_clock) then
			clk <= '1';
			wait for CLK_PERIOD/2;
			clk <= '0';
			wait for CLK_PERIOD/2;
		else
			wait;
		end if;
	end process clkproc;
	
	stimproc:process
	begin
		reset <= '1';
		wait for CLK_PERIOD;
		reset <= '0';
		
		regins(3) <= x"00A5";
		
		addr <= x"01";
		datain <= x"00AA" ;
		
		w_en <= '1';
		wait for CLK_PERIOD;
		w_en <= '0';
		
		wait for CLK_PERIOD;
		
		addr <= x"02";
		datain <= x"0055" ;
		
		w_en <= '1';
		wait for CLK_PERIOD;
		w_en <= '0';
		
		wait for CLK_PERIOD;
		
		addr <= x"03";
		r_en <= '1';
		wait for CLK_PERIOD;
		r_en <= '0';
		
		
		wait for CLK_PERIOD*2;
		stop_clock <= true;
		wait;
	end process stimproc;

end behav;