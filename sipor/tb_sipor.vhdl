library ieee;
use ieee.std_logic_1164.all;

entity tb_sipor is
end entity;

architecture behav of tb_sipor is
	
	signal clk:std_logic;
	signal reset:std_logic;
	signal stop_clock:boolean := false;
	constant CLK_PERIOD:time := 10 ns;
	
	constant n:integer := 6;
	
	signal serial:std_logic := '0';
	signal parallel:std_logic_vector(n-1 downto 0) := (others=>'0');
	signal pclk:std_logic;
	
begin

	uut:entity work.sipor(behav)
		generic map(n)
		port map(
			serial,
			parallel,
			pclk,
			clk,
			reset
		);
	
	ulfsr:entity work.lfsr
		generic map(6)
		port map(
			open,
			serial,
			clk,
			reset
		);

	clkproc:process
	begin
		if (not stop_clock) then
			clk <= '0';
			wait for CLK_PERIOD/2;
			clk <= '1';
			wait for CLK_PERIOD/2;
		else
			wait;
		end if;
	end process clkproc;
	
	stimproc:process
	begin
		
		reset <= '1';
		wait for CLK_PERIOD;
		reset <= '0';
		
		wait for CLK_PERIOD*50;
	
		stop_clock <= true;
		wait;
	end process stimproc;

end behav;