--
--	Serial Input Parallel Output register
--	
--	by Hasan Yavuz �ZDERYA ( hy [at] ozderya.net ) 
--	Trabzon, KTU DSPLAB 17.05.2013
--	

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sipor is
	
	Generic( n			: integer := 4;
			 lsbfirst	: boolean := true);
	
	Port( sin	: in	std_logic;	-- serial in
		  pout	: out 	std_logic_vector(n-1 downto 0);	-- parallel output
		  oclk	: out	std_logic;	-- parallel output clock
		  clk	: in	std_logic;	-- serial input clock
		  reset	: in	std_logic
		 );
		  	
end entity sipor;

architecture behav of sipor is

begin
	
	process(clk,reset)
		variable reg: std_logic_vector(n-1 downto 0);
		variable count: integer range 0 to n := 0;
	begin
		
		if reset = '1' then
			
			reg := (others => '0');
			pout <= (others => '0');
			oclk <= '0';
			count := 0;
						
		elsif rising_edge(clk) then
			
			if lsbfirst then
				reg(n-2 downto 0) := reg(n-1 downto 1);
				reg(n-1) := sin;
			else	-- msbfirst
				reg(n-1 downto 1) := reg(n-2 downto 0);
				reg(0) := sin;
			end if;
			
			count := count+1;
			
			if count = n/2 then
				
				oclk <= '0';
				
			elsif count = n then
				
				count := 0;
				
				oclk <= '1';
				pout <= reg;
				
			end if;
			
		end if;
		
	end process;
	
end architecture behav;