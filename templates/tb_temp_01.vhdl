library ieee;
use ieee.std_logic_1164.all;

entity temp_entity is
end entity;

architecture behav of temp_entity is
	
	signal clk:std_logic;
	signal reset:std_logic;
	signal stop_clock:boolean := false;
	constant CLK_PERIOD:time := 10 ns;
	
begin

	uut:entity work.entity_name
		generic map()
		port map(
			,
			clk,
			reset
		);

	clkproc:process
	begin
		if (not stop_clock) then
			clk <= '1';
			wait for CLK_PERIOD/2;
			clk <= '0';
			wait for CLK_PERIOD/2;
		else
			wait;
		end if;
	end process clkproc;
	
	stimproc:process
	begin
		reset <= '1';
		wait for CLK_PERIOD;
		reset <= '0';
	
		stop_clock <= true;
		wait;
	end process stimproc;

end behav;