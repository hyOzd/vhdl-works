--
--	Entity Template
--
--	by Hasan Yavuz �ZDERYA ( hy [at] ozderya.net )
--
--	<place> , <date>
--
--	<description>

library ieee;
use ieee.std_logic_1164.all;
--use ieee.numeric_std.all;
--use ieee.math_real.all;

entity t_entity_name is
	
	Generic( );
	
	Port(...
		 clk	: in	std_logic;
		 reset	: in	std_logic
		 );
	
end entity;

architecture rtl of t_entity_name is
	
	--type array_type is array(natural range<>) of std_logic_vector(n-1 downto 0);
	--subtype the_type is std_logic_vector(n-1 downto 0);
	
begin

	process(clk,reset)
	begin
		
		if reset = '1' then
		
		elsif rising_edge(clk) then
		
		end if;	
		
	end process;

end architecture rtl;