library ieee;
use ieee.std_logic_1164.all;

entity spim is
  
  generic (
    n            : integer := 8;            -- word size
    mode         : integer := 3;            -- CPOL = 1,  CPHA = 1
    numof_slaves : integer := 8             -- number of slaves
    );

  port (
    data_in   : in  std_logic_vector(n-1 downto 0);
    data_out  : in  std_logic_vector(n-1 downto 0);
    en        : in  std_logic;
    slave_sel : in  std_logic_vector(n-1 downto 0);
    SSEL      : out std_logic_vector(n-1 downto 0);
    MOSI      : out std_logic;
    MISO      : in  std_logic;
    SCLK      : out std_logic;
    clk       : in  std_logic;       -- module clock
    rst       : in  std_logic        -- module reset
    );

end entity spim;

architecture rtl of spim is
begin  -- architecture rtl

    

end architecture rtl;
