library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ieee_proposed;
use ieee_proposed.fixed_pkg.all;

entity tb_cordic_gen is
end entity;

architecture behav of tb_cordic_gen is
	
	signal clk:std_logic;
	signal stop_clock:boolean := false;
	constant CLK_PERIOD:time := 10 ns;
	
	constant n:integer := 16;
	
	signal cos,sin : sfixed(0 downto -(n-1));
	signal r_cos,r_sin : real := 0.0;
	
	signal phaseinc: sfixed(3 downto -(n-4)) := (others => '0');
	signal reset:std_logic := '0';
	
begin

	uut:entity work.cordic_gen
		generic map(n,16)
		port map(
				phaseinc,
				cos,sin,
				clk,
				reset);

	clkproc:process
	begin
		if (not stop_clock) then
			clk <= '0';
			wait for CLK_PERIOD/2;
			clk <= '1';
			wait for CLK_PERIOD/2;
		else
			wait;
		end if;
	end process clkproc;
	
	r_cos <= to_real(cos);
	r_sin <= to_real(sin);
	
	stimproc:process
	begin
		
		reset <= '1';
		wait for CLK_PERIOD;
		reset <= '0';
		
		phaseinc <= to_sfixed(MATH_PI / 10.0,phaseinc);
					
		wait for CLK_PERIOD*30;
		
		phaseinc <= to_sfixed(MATH_PI / 20.0,phaseinc);
		
		wait for CLK_PERIOD*60;
		
		stop_clock <= true;
		wait;
	end process stimproc;

end behav;