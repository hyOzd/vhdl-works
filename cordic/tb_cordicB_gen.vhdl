library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity tb_cordicB_gen is
end entity;

architecture behav of tb_cordicB_gen is
	
	signal clk:std_logic;
	signal stop_clock:boolean := false;
	constant CLK_PERIOD:time := 10 ns;
	
	constant n:integer := 16;
	subtype tfixed is signed(n-1 downto 0);
	
	-- scalars for moving fixed numbers to integers
	constant fx_c1		: real := 2**(real(n)-2.0);	-- for Q1.14
	constant fx_c2		: real := 2**(real(n)-4.0);  -- for Q3.12
	
	signal cos,sin : tfixed;
	signal r_cos,r_sin : real := 0.0;
	
	signal phaseinc: tfixed := (others => '0');
	signal reset:std_logic := '0';
	
begin

	uut:entity work.cordicB_gen
		generic map(n,16)
		port map(
				phaseinc,
				cos,sin,
				clk,
				reset);

	clkproc:process
	begin
		if (not stop_clock) then
			clk <= '0';
			wait for CLK_PERIOD/2;
			clk <= '1';
			wait for CLK_PERIOD/2;
		else
			wait;
		end if;
	end process clkproc;
	
	r_cos <= real(to_integer(cos))/fx_c1;
	r_sin <= real(to_integer(sin))/fx_c1;
	
	stimproc:process
	begin
		
		reset <= '1';
		wait for CLK_PERIOD;
		reset <= '0';
		
		phaseinc <= to_signed(integer(MATH_PI* fx_c2 / 100.0 ),n);
					
		wait for CLK_PERIOD*600;
		
		phaseinc <= to_signed(integer(MATH_PI* fx_c2 / 20.0 ),n);
		
		wait for CLK_PERIOD*60;
		
		stop_clock <= true;
		wait;
	end process stimproc;

end behav;