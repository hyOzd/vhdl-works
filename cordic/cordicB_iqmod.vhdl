--
--	CORDIC IQ Modulator module (using Cordic-B module)
--
--	by Hasan Yavuz �ZDERYA ( hy [at] ozderya.net )
--	
--	Trabzon KTU DSPLAB, 17.05.2013
--
--	Description:
--
--	Let's say input is a+bj (base modulated symbols, such as m-qam)
--	multiply this with a complex signal which is 1<p (1: amplitude, p:phase )
--	result is, y=(a*cos(p)-b*sin(p))+(a*sin(p)+b*cos(p))j
--	CORDIC is basically a complex multiplier. If you look at above result
--	you will see that the imaginary part is what we are looking for as
--	output of IQ-Modulator. That means we can use imaginary output of CORDIC
--	as output of an IQ-Modulator.
--
--	Remember, we swap real and complex parts in inputs to modulate I with cos,
--	Q with sin carrier.
--
	

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity cordicB_iqmod is
	
	Generic(
		n:natural := 16;
		ns:natural := 10	-- number of iterative cordic stages
	);
	
	Port(
		phaseinc	: in	signed(n-1 downto 0);	-- phase increment value, determines frequency
		I			: in	signed(n-1 downto 0);	-- in-phase input
		Q			: in	signed(n-1 downto 0);	-- quad-phase input
		modulated	: out	signed(n-1 downto 0);	-- output: s_i*cos(p)+s_q*sin(p)
		clk			: in	std_logic;
		reset		: in	std_logic
	);
	
	subtype tfixed is signed(n-1 downto 0);
	
end cordicB_iqmod;

architecture rtl of cordicB_iqmod is

	signal phase: tfixed := (others => '0');
	
	-- scalars for moving fixed numbers to integers
	constant fx_c1		: real := 2**(real(n)-2.0);	-- for Q1.14
	constant fx_c2		: real := 2**(real(n)-4.0);  -- for Q3.12
	
	constant fx_2pi		: tfixed := to_signed(integer(2.0*MATH_PI*fx_c2),n);
			
	-- cordic module outputs
	signal cord_r,cord_c : tfixed := to_signed(0,n);
		
begin
			
	ucordic: entity work.cordicB
		generic map (n,ns)
		port map(
			phase,
			Q,I,
			cord_r,cord_c,
			clk,
			reset
		);
	
	process(clk,reset)
		variable v_phase : tfixed;
	begin
		if reset = '1' then
			
			phase <= to_signed(0,n);
			modulated <= to_signed(0,n);
			
		elsif rising_edge(clk) then
			
			v_phase := phase;
			
			v_phase := v_phase + phaseinc;			
			if v_phase > fx_2pi then
				v_phase := v_phase-fx_2pi;
			end if;
			
			phase <= v_phase;
			
			-- output latch
			modulated <= cord_c;
			
		end if;
	end process;
		
end rtl;
