--
--
--	CORDIC sine-cosine generator module by Hasan Yavuz �ZDERYA
--	
--	Trabzon KTU DSPLAB, 15.04.2013
--


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ieee_proposed;
use ieee_proposed.fixed_pkg.all;

entity cordic_gen is
	
	Generic(n:natural := 16; ns:natural := 10);
	
	Port(
		phasei	: in	sfixed(3 downto -(n-4));	-- phase increment value, determines period
		cos		: out	sfixed(0 downto -(n-1));	-- outputs
		sin		: out	sfixed(0 downto -(n-1));
		clk		: in	std_logic;
		reset	: in	std_logic
	);
	
	subtype tfixed is sfixed(0 downto -(n-1));		-- type used for internals, this is different than 'cordic' module
	subtype rtfixed is sfixed(3 downto -(n-4));		-- type used for radians
	
end cordic_gen;

architecture rtl of cordic_gen is

	signal phase: rtfixed := (others => '0');
	
	-- dummy signal for type conversions
	signal d_tfixed : tfixed;
	
	-- constant signals for inputs
	signal the_one: tfixed := to_sfixed(1,d_tfixed);
	signal the_zero: tfixed := to_sfixed(0,d_tfixed);
	
	-- output
	signal s_cos,s_sin : tfixed := to_sfixed(0,d_tfixed);
	
begin
		
	sin <= s_sin;
	cos <= s_cos;
	
	ucordic: entity work.cordic
		generic map (n,ns)
		port map(
			phase,
			the_one,the_zero,
			s_cos,s_sin,
			clk,
			reset
		);
	
	-- phase increment process
	process(clk,reset)
	begin
		if reset = '1' then
			
			phase <= to_sfixed(0,phase);
			
		elsif rising_edge(clk) then
			
			phase <= resize(phase + phasei,phase);
			
			if phase > to_sfixed(2.0*MATH_PI,phase) then
				phase <= to_sfixed(0,phase);
			end if;
			
		end if;
	end process;
	
	
end rtl;
