library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ieee_proposed;
use ieee_proposed.fixed_pkg.all;

entity tb_cordic is
end entity;

architecture behav of tb_cordic is
	
	signal clk:std_logic;
	signal stop_clock:boolean := false;
	constant CLK_PERIOD:time := 10 ns;
	
	constant n:integer := 32;
	
	signal rotation: sfixed(3 downto -n+4) := (others => '0');
	signal d_r,d_c: sfixed(0 downto -n+1) := (others => '0');
	signal q_r,q_c: sfixed(0 downto -n+1) := (others => '0');
	signal q_r_r,q_c_r:real := 0.0;
	signal reset:std_logic;
	
begin

	uut:entity work.cordic(single_unit)
		generic map(n,16)
		port map(
				rotation,
				d_r,d_c,
				q_r,q_c,
				clk,
				reset);

	clkproc:process
	begin
		if (not stop_clock) then
			clk <= '0';
			wait for CLK_PERIOD/2;
			clk <= '1';
			wait for CLK_PERIOD/2;
		else
			wait;
		end if;
	end process clkproc;
	
	q_r_r <= to_real(q_r);
	q_c_r <= to_real(q_c);
	
	stimproc:process
	begin
		
		reset <= '1';
		wait for CLK_PERIOD;
		reset <= '0';
		
		d_r <= to_sfixed(1,d_r);
		d_c <= to_sfixed(0,d_r);
		
		for i in 0 to 360 loop
			rotation <= to_sfixed(real(i)*MATH_PI/180.0,rotation);
			wait for CLK_PERIOD;
		end loop;
			
		wait for CLK_PERIOD*30;
		
		stop_clock <= true;
		wait;
	end process stimproc;

end behav;