--
--
--	CORDIC-B module by Hasan Yavuz �ZDERYA ( hy [at] ozderya.net )
--	
--	Trabzon KTU DSPLAB 2013
--	
--	Desc: This module is implemented with numeric_std's signed type
--	rather than sfixed. Also input Q formats are a bit different (Q.15->Q1.14)
--	
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

-- all internals work with Q1.14 except
-- numbers that hold radians are Q3.12

entity cordicB is
	
	Generic(n:integer := 16;	-- bus width (n=16 -> Q1.14), internals work with Q1.14
			ns:integer := 10);	-- number of stages
	Port(
		rotation: in	signed(n-1 downto 0);	-- rotation input
		d_r		: in	signed(n-1 downto 0);	-- inputs
		d_c		: in	signed(n-1 downto 0);
		q_r		: out	signed(n-1 downto 0);	-- outputs
		q_c		: out	signed(n-1 downto 0);
		clk		: in	std_logic;
		reset	: in	std_logic
	);
	
	subtype tfixed is signed(n-1 downto 0);
		
end entity cordicB;

architecture rtl of cordicB is

	type tfixed_stages is array(-1 to ns-1) of tfixed;
	type tfixed_array is array(Natural range <>) of tfixed;

	-- stage input output signals
	
	-- phi: target rotation
	-- beta: rotation so far
	signal 	s_beta,s_phi,											
			s_beta_o,s_phi_o : tfixed_stages := (others => (others => '0'));	
	
	-- d_r,d_c: stage input real and complex		
	-- q_r,q_c: stage output real and complex
	signal 	s_d_r, s_d_c,										
			s_q_r, s_q_c : tfixed_stages := (others => (others => '0'));
				
	-- constants
	signal atans	: tfixed_array(0 to ns-1);	-- holds arctangents of 2^-i (i=0,1,2..)
	
	-- scalars for moving fixed numbers to integers
	constant fx_c1		: real := 2**(real(n)-2.0);	-- for Q1.14
	constant fx_c2		: real := 2**(real(n)-4.0);  -- for Q3.12
	
	constant out_c		: tfixed := to_signed(integer(0.607253031529134*fx_c1),n);
	constant pi_3_2		: tfixed := to_signed(integer(MATH_3_PI_OVER_2*fx_c2),n);
	constant pi			: tfixed := to_signed(integer(MATH_PI*fx_c2),n);
	constant pi_1_2		: tfixed := to_signed(integer(MATH_PI_OVER_2*fx_c2),n);
	constant pi_1_4		: tfixed := to_signed(integer(MATH_PI_OVER_4*fx_c2),n);

begin
	
	-- generate required constants for iterative stages atan(2^-i)
	gen_atans: for i in 0 to ns-1 generate
		constant c_atan : real := arctan(2.0**(-i))*fx_c2;
	begin
		atans(i) <= to_signed(integer(c_atan),n);
	end generate;
	
	-- generate intertal connections between stages
	gen_internals:for i in 0 to ns-1 generate  -- except first stage which is input
				
		s_beta(i) <= s_beta_o(i-1);
		s_phi(i) <= s_phi_o(i-1);
		
		s_d_r(i) <= s_q_r(i-1);
		s_d_c(i) <= s_q_c(i-1);
					
	end generate;
	
	process(clk,reset)
		variable a,b,p,k	: tfixed;
		variable beta,phi	: tfixed;
		variable q_r1,q_c1		: signed(2*n-1 downto 0);
	begin
		
		if reset = '1' then
			q_r <= to_signed(0,n);	
			q_c <= to_signed(0,n);
			s_beta_o <= (others => to_signed(0,n));
			s_phi_o <= (others => to_signed(0,n));
			s_q_r <= (others => to_signed(0,n));
			s_q_c <= (others => to_signed(0,n));
		elsif rising_edge(clk) then
			
			-- register inputs for first stage
			s_beta(-1) <= to_signed(0,n);
			s_phi(-1) <= rotation;
			s_d_r(-1) <= d_r;
			s_d_c(-1) <= d_c;
			
			for i in -1 to ns-1 loop
				
				a := s_d_r(i);
				b := s_d_c(i);
				
				beta := s_beta(i);
				phi := s_phi(i);
				
				if i = -1 then 	-- jumper stage for r > pi/2
					
					if phi > pi_3_2 then
						p := b;
						k := -a;
						phi := phi-pi_3_2;
					elsif phi > pi then
						p := -a;
						k := -b;
						phi := phi-pi;
					elsif phi > pi_1_2 then
						p := -b;
						k := a;
						phi := phi-pi_1_2;
					else
						p := a;
						k := b;
					end if;	
					
				else			-- iterative stages
					
					if  phi>beta then
						
						p := a-shift_right(b,i);
						k := b+shift_right(a,i);
						beta := beta+atans(i);
						
					else
						
						p := a+shift_right(b,i);
						k := b-shift_right(a,i);
						beta := beta-atans(i);
						
					end if;
					
				end if;
				
				s_q_r(i) <= p;
				s_q_c(i) <= k;
				
				s_beta_o(i) <= beta;
				s_phi_o(i) <= phi;
				
			end loop;
			
			-- last stage to output notice output scalar (out_c)
			q_r1 := out_c*s_q_r(ns-1);
			q_c1 := out_c*s_q_c(ns-1);
			
			q_r <= q_r1(2*n-3 downto n-2);
			q_c <= q_c1(2*n-3 downto n-2);
			
		end if;
		
	end process;
	
end rtl;