--
--
--	CORDIC sine-cosine generator module (using Cordic-B module) by Hasan Yavuz �ZDERYA
--	
--	Trabzon KTU DSPLAB, 15.04.2013
--
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity cordicB_gen is
	
	Generic(n:natural := 16; ns:natural := 10);
	
	Port(
		phasei	: in	signed(n-1 downto 0);	-- phase increment value, determines period
		cos		: out	signed(n-1 downto 0);	-- outputs
		sin		: out	signed(n-1 downto 0);
		clk		: in	std_logic;
		reset	: in	std_logic
	);
	
	subtype tfixed is signed(n-1 downto 0);
	
end cordicB_gen;

architecture rtl of cordicB_gen is

	signal phase: tfixed := (others => '0');
	
	-- scalars for moving fixed numbers to integers
	constant fx_c1		: real := 2**(real(n)-2.0);	-- for Q1.14
	constant fx_c2		: real := 2**(real(n)-4.0);  -- for Q3.12
	
	constant fx_2pi		: tfixed := to_signed(integer(2.0*MATH_PI*fx_c2),n);
		
	-- constant signals for inputs
	signal the_one: tfixed := to_signed(integer(1.0*fx_c1),n);
	signal the_zero: tfixed := to_signed(0,n);
	
	-- output
	signal s_cos,s_sin : tfixed := to_signed(0,n);
		
begin
		
	sin <= s_sin;
	cos <= s_cos;
	
	ucordic: entity work.cordicB
		generic map (n,ns)
		port map(
			phase,
			the_one,the_zero,
			s_cos,s_sin,
			clk,
			reset
		);
	
	-- phase increment process
	process(clk,reset)
		variable v_phase : tfixed;
	begin
		if reset = '1' then
			
			phase <= to_signed(0,n);
			
		elsif rising_edge(clk) then
			
			v_phase := phase;
			
			v_phase := v_phase + phasei;			
			if v_phase > fx_2pi then
				v_phase := v_phase-fx_2pi;
			end if;
			
			phase <= v_phase;
			
		end if;
	end process;
	
end rtl;
