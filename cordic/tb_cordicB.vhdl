library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity tb_cordicB is
end entity;

architecture behav of tb_cordicB is
	
	signal clk:std_logic;
	signal stop_clock:boolean := false;
	constant CLK_PERIOD:time := 10 ns;
	
	constant n:integer := 16;
	
	-- scalars for moving fixed numbers to integers
	constant fx_c1		: real := 2**(real(n)-2.0);	-- for Q1.14
	constant fx_c2		: real := 2**(real(n)-4.0);  -- for Q3.12
	
	subtype tfixed is signed(n-1 downto 0);
	
	signal rotation: tfixed := (others => '0');
	signal d_r,d_c: tfixed := (others => '0');
	signal q_r,q_c: tfixed := (others => '0');
	signal q_r_r,q_c_r:real := 0.0;
	signal reset:std_logic;
	
begin

	uut:entity work.cordicB(rtl)
		generic map(n,16)
		port map(
				rotation,
				d_r,d_c,
				q_r,q_c,
				clk,
				reset);

	clkproc:process
	begin
		if (not stop_clock) then
			clk <= '0';
			wait for CLK_PERIOD/2;
			clk <= '1';
			wait for CLK_PERIOD/2;
		else
			wait;
		end if;
	end process clkproc;
	
	q_r_r <= real(to_integer(q_r))/fx_c1;
	q_c_r <= real(to_integer(q_c))/fx_c1;
	
	stimproc:process
	begin
		
		reset <= '1';
		wait for CLK_PERIOD;
		reset <= '0';
		
		d_r <= to_signed(integer(1.0*fx_c1),n);
		d_c <= to_signed(integer(0.0*fx_c1),n);
		
		for i in 0 to 360 loop 
			rotation <= to_signed(integer(real(i)*MATH_PI/180.0*fx_c2),n);
			wait for CLK_PERIOD;
		end loop;
			
		wait for CLK_PERIOD*30;
		
		stop_clock <= true;
		wait;
	end process stimproc;

end behav;