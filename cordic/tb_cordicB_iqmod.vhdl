library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity tb_cordicB_iqmod is
end entity;

architecture behav of tb_cordicB_iqmod is
	
	signal clk:std_logic;
	signal reset:std_logic;
	signal stop_clock:boolean := false;
	constant CLK_PERIOD:time := 10 ns;
	
	constant n:integer := 16;
	subtype tfixed is signed(n-1 downto 0);
	
	-- scalars for moving fixed numbers to integers
	constant fx_c1		: real := 2**(real(n)-2.0);	-- for Q1.14
	constant fx_c2		: real := 2**(real(n)-4.0);  -- for Q3.12
	
	-- bit clock and symbol clock (clks=clkb/2)
	signal clkb,clks:std_logic;
	constant BCLK_PER:time := CLK_PERIOD*20;
	signal ser_bits:std_logic := '0';
	signal par_bits:std_logic_vector(1 downto 0);
	
	signal mod_r,mod_c,modulated : tfixed;
	signal r_modulated:real;
	
	signal phaseinc: tfixed := (others => '0');
	
begin
	
	r_modulated <= real(to_integer(modulated))/fx_c1;
	
	uut:entity work.cordicB_iqmod
		generic map(n,16)
		port map(
			phaseinc,
			mod_r,mod_c,
			modulated,
			clk,
			reset
		);
	
	-- random bit seq. generator
	urbg:entity work.lfsr
		generic map(16,45)
		port map(
			open,
			ser_bits,
			clkb,
			reset
		);
	
	-- serial to parallel
	usipor:entity work.sipor
		generic map(2)
		port map(
			ser_bits,
			par_bits,
			clks,
			clkb,
			reset
		);
	
	-- qpsk modulator
	uqpsk:entity work.qpskmod
		generic map(n)
		port map(
			par_bits,
			mod_r,mod_c,
			clk,
			reset
		);
		
	clkproc:process
	begin
		if (not stop_clock) then
			clk <= '1';
			wait for CLK_PERIOD/2;
			clk <= '0';
			wait for CLK_PERIOD/2;
		else
			wait;
		end if;
	end process clkproc;
	
	bclkproc:process
	begin
		if (not stop_clock) then
			clkb <= '1';
			wait for BCLK_PER/2;
			clkb <= '0';
			wait for BCLK_PER/2;
		else
			wait;
		end if;
	end process;
	
	stimproc:process
	begin
		reset <= '1';
		wait for CLK_PERIOD;
		reset <= '0';
		
		phaseinc <= to_signed(integer(MATH_PI* fx_c2 / 100.0 ),n);
		
		wait for BCLK_PER*200;
	
		stop_clock <= true;
		wait;
	end process stimproc;

end behav;