-- Tests for complex_signed package
library std;
use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;
use ieee.math_complex.all;

library work;
use work.complex_signed.all;

entity test_complex_signed is
-- TEST ENTITY
end entity test_complex_signed;

architecture test of test_complex_signed is

    -- number of bits
    subtype cs8 is complex_signed(7 downto 0);
    subtype cs4 is complex_signed(3 downto 0);
    subtype s8 is signed(7 downto 0);

    function cs (re : integer; im : integer)
        return cs8 is
    begin
        return to_complex_signed(re, im, 8);
    end function cs;

    function tcs4 (re : integer; im : integer)
        return cs4 is
    begin
        return to_complex_signed(re, im, 4);
    end function tcs4;

    function ts8 (val : integer)
        return s8 is
    begin
        return to_signed(val, 8);
    end function ts8;

    -- test data type for to_std_logic_vector
    type test_slv_data_record is record
        arg : cs4;
        expected : std_logic_vector(7 downto 0);
    end record test_slv_data_record;

    type test_slv_data is array (natural range <>) of test_slv_data_record;

    -- test data type for operator test
    type test_data_record is record
        left, right, expected : cs8;
    end record test_data_record;

    type test_data_type is array (natural range <>) of test_data_record;

    -- test data type for multiplication operator
    type test_mult_data_record is record
        left, right : cs4;
        expected : complex_signed(8 downto 0);
    end record test_mult_data_record;

    type test_mult_data_type is array (natural range <>) of test_mult_data_record;

    -- test data type for multiplication operator "*"(complex_signed, signed)
    type test_mul_data_cs_record is record
        left : cs8;
        right : s8;
        expected : complex_signed(15 downto 0);
    end record test_mul_data_cs_record;

    type test_mul_data_cs_type is array (natural range <>) of test_mul_data_cs_record;

    -- test data type for multiplication operator "*"(complex_signed, integer)
    type test_mul_data_ci_record is record
        left : cs8;
        right : integer;
        expected : complex_signed(15 downto 0);
    end record test_mul_data_ci_record;

    type test_mul_data_ci_type is array (natural range <>) of test_mul_data_ci_record;

    -- test data for division operator "/"(complex_signed, signed)
    type test_div_cs_data_record is record
        left : cs8;
        right : signed(7 downto 0);
        expected : cs8;
    end record test_div_cs_data_record;

    type test_div_cs_data_type is array (natural range<>) of test_div_cs_data_record;

    -- test data for division operator "/"(complex_signed, integer)
    type test_div_ci_data_record is record
        left : cs8;
        right : integer;
        expected : cs8;
    end record test_div_ci_data_record;

    type test_div_ci_data_type is array (natural range<>) of test_div_ci_data_record;

    -- test data for unary minus operator "-"
    type test_uminus_data_record is record
        arg : cs8;
        expected : cs8;
    end record test_uminus_data_record;

    type test_uminus_data is array (natural range<>)  of test_uminus_data_record;

    -- test data type for shifting functions
    type test_shift_data_record is record
        arg : cs8;
        count : natural;
        expected : cs8;
    end record test_shift_data_record;

    type test_shift_data is array (natural range <>) of test_shift_data_record;

    -- test data type for comparison operator tests
    type test_comp_data_record is record
        left, right : cs8;
        expected : boolean;
    end record test_comp_data_record;

    type test_comp_data_type is array (natural range <>) of test_comp_data_record;

    -- test data type for comparison operator tests with different sized inputs
    type test_comp_data_84_record is record
        left : cs8;
        right : cs4;
        expected : boolean;
    end record test_comp_data_84_record;

    type test_comp_data_84_type is array (natural range <>) of test_comp_data_84_record;

    -- test data for to_std_logic_vector function
    constant td_slv : test_slv_data(0 to 9) :=
        (
            ( tcs4(0, 0), "00000000"),
            ( tcs4(1, 1), "00010001"),
            ( tcs4(7, 7), "01110111"),
            ( tcs4(-1, -1), "11111111"),
            ( tcs4(-2, -2), "11101110"),
            ( tcs4(-3, -3), "11011101"),
            ( tcs4(-8, -8), "10001000"),
            ( tcs4(-8, 0), "10000000"),
            ( tcs4(-3, 7), "11010111"),
            ( tcs4(3, -3), "00111101")
        );

    -- test data for "+" operator
    constant td_add : test_data_type(0 to 9) :=
        (
            ( cs(0, 0), cs(0, 0), cs(0, 0) ),
            ( cs(1, 0), cs(4, 0), cs(5, 0) ),
            ( cs(2, 3), cs(5, 8), cs(7, 11) ),
            ( cs(10, 20), cs(10, 20), cs(20, 40) ),
            ( cs(12, 56), cs(98, 54), cs(110, 110) ),
            ( cs(72, 0), cs(33, 0), cs(105, 0) ),
            ( cs(0, 55), cs(4, 72), cs(4, 127) ),
            ( cs(-18, 0), cs(0, -94), cs(-18, -94) ),
            ( cs(-120, 10), cs(120, -16), cs(0, -6) ),
            ( cs(-27, -100), cs(-100, -27), cs(-127, -127) )
        );

    -- NEGATIVE test data for "+" operator
    constant td_add_n : test_data_type(0 to 3) :=
        (
            ( cs(12, 13), cs(92, 11), cs(12, 3) ),
            ( cs(12, 13), cs(92, 11), cs(92, 11) ),
            ( cs(96, 13), cs(12, 87), cs(0, 0) ),
            ( cs(96, 13), cs(-12, -98), cs(108, 111) )
        );

    constant td_sub : test_data_type(0 to 9) :=
        (
            ( cs(0, 0), cs(0, 0), cs(0, 0) ),
            ( cs(1, 0), cs(4, 0), cs(-3, 0) ),
            ( cs(2, 3), cs(5, 8), cs(-3, -5) ),
            ( cs(10, 20), cs(10, 20), cs(0, 0) ),
            ( cs(12, 56), cs(98, 54), cs(-86, 2) ),
            ( cs(72, 0), cs(33, 0), cs(39, 0) ),
            ( cs(0, 55), cs(4, 72), cs(-4, -17) ),
            ( cs(-18, 0), cs(0, -94), cs(-18, 94) ),
            ( cs(-12, 10), cs(20, -16), cs(-32, 26) ),
            ( cs(-27, -100), cs(100, 27), cs(-127, -127) )
        );

    -- test data for multiplication operator
    constant td_mul : test_mult_data_type(0 to 11) :=
        (
            ( tcs4(0, 0), tcs4(0, 0), to_cs(0, 0, 9) ),
            ( tcs4(1, 1), tcs4(0, 0), to_cs(0, 0, 9) ),
            ( tcs4(0, 0), tcs4(1, 1), to_cs(0, 0, 9) ),
            ( tcs4(1, 1), tcs4(1, 1), to_cs(0, 2, 9) ),
            ( tcs4(1, 1), tcs4(5, 1), to_cs(4, 6, 9) ),
            ( tcs4(5, 3), tcs4(7, 2), to_cs(29, 31, 9) ),
            ( tcs4(-6, -2), tcs4(7, -7), to_cs(-56, 28, 9) ),
            ( tcs4(7, 7), tcs4(7, 7), to_cs(0, 98, 9) ),
            ( tcs4(7, 7), tcs4(7, -7), to_cs(98, 0, 9) ),
            ( tcs4(7, 7), tcs4(-7, -7), to_cs(0, -98, 9) ),
            ( tcs4(7, -7), tcs4(-7, -7), to_cs(-98, 0, 9) ),
            ( tcs4(-7, -7), tcs4(-7, -7), to_cs(0, 98, 9) )
        );

    -- test data for multiplication of complex_signed * signed
    constant td_mul_cs : test_mul_data_cs_type(0 to 3) :=
        (
            ( cs(0, 0), ts8(1), to_cs(0, 0, 16) ),
            ( cs(1, 1), ts8(1), to_cs(1, 1, 16) ),
            ( cs(4, 4), ts8(1), to_cs(4, 4, 16) ),
            ( cs(10, -10), ts8(5), to_cs(50, -50, 16) )
        );

    -- test data for multiplication of complex_signed * integer
    constant td_mul_ci : test_mul_data_ci_type(0 to 3) :=
        (
            ( cs(0, 0), 1, to_cs(0, 0, 16) ),
            ( cs(5, 5), 1, to_cs(5, 5, 16) ),
            ( cs(5, -5), 5, to_cs(25, -25, 16) ),
            ( cs(15, -5), -4, to_cs(-60, 20, 16) )
        );

    -- test data for division operator "/"(complex_signed, signed)
    constant td_div_cs : test_div_cs_data_type(0 to 3) :=
        (
            ( cs(0, 0) , to_signed(1, 8), cs(0, 0)),
            ( cs(1, 1) , to_signed(1, 8), cs(1, 1)),
            ( cs(10, 10) , to_signed(2, 8), cs(5, 5)),
            ( cs(9, 15) , to_signed(3, 8), cs(3, 5))
        );

    -- test data for division operator "/"(complex_signed, integer)
    constant td_div_ci : test_div_ci_data_type(0 to 3) :=
        (
            ( cs(0, 0), 1, cs(0, 0) ),
            ( cs(1, 1), 1, cs(1, 1) ),
            ( cs(4, 2), 2, cs(2, 1) ),
            ( cs(20, 10), 5, cs(4, 2) )
        );

    -- test data type for unary minus operator "-"
    constant td_uminus : test_uminus_data (0 to 5) :=
        (
            ( cs(0, 0), cs(0, 0) ),
            ( cs(1, 1), cs(-1, -1) ),
            ( cs(5, 5), cs(-5, -5) ),
            ( cs(5, -5), cs(-5, 5) ),
            ( cs(-5, 5), cs(5, -5) ),
            ( cs(7, 7), cs(-7, -7) )
        );

    -- test data for shift_right function
    constant td_shift_right : test_shift_data(0 to 12) :=
        (
            ( cs(0, 0), 0, cs(0, 0)),
            ( cs(0, 0), 1, cs(0, 0)),
            ( cs(1, 1), 1, cs(0, 0)),
            ( cs(2, 2), 1, cs(1, 1)),
            ( cs(24, 24), 2, cs(6, 6)),
            ( cs(24, -24), 2, cs(6, -6)),
            ( cs(-24, -24), 2, cs(-6, -6)),
            ( cs(-24, -32), 2, cs(-6, -8)),
            ( cs(-24, 32), 2, cs(-6, 8)),
            ( cs(-24, 32), 0, cs(-24, 32)),
            ( cs(0, 0), 8, cs(0, 0)),
            ( cs(97, 36), 8, cs(0, 0)),
            ( cs(-97, -36), 8, cs(-1, -1))
        );

    -- test data for shift_left function
    constant td_shift_left : test_shift_data(0 to 10) :=
        (
            ( cs(0, 0), 0, cs(0, 0)),
            ( cs(0, 0), 1, cs(0, 0)),
            ( cs(1, 1), 1, cs(2, 2)),
            ( cs(2, 2), 1, cs(4, 4)),
            ( cs(1, 1), 6, cs(64, 64)),
            ( cs(3, 3), 2, cs(12, 12)),
            ( cs(3, -3), 2, cs(12, -12)),
            ( cs(4, -3), 2, cs(16, -12)),
            ( cs(-4, -3), 2, cs(-16, -12)),
            ( cs(-1, -1), 6, cs(-64, -64)),
            ( cs(-1, -1), 7, cs(-128, -128))
        );

    -- test data for "=" comparison operator
    constant td_equ : test_comp_data_type(0 to 13) :=
        (
            ( cs(0, 0), cs(0, 0), true ),
            ( cs(10, 10), cs(10, 10), true ),
            ( cs(127, 127), cs(127, 127), true ),
            ( cs(127, 0), cs(127, 0), true ),
            ( cs(0, 127), cs(0, 127), true ),
            ( cs(-127, -127), cs(-127, -127), true ),
            ( cs(-10, -10), cs(-10, -10), true ),
            ( cs(-10, -65), cs(-10, -65), true ),
            ( cs(0, 10), cs(0, 11), false ),
            ( cs(0, 10), cs(10, 10), false ),
            ( cs(20, 10), cs(-20, 10), false ),
            ( cs(20, 10), cs(20, -10), false ),
            ( cs(-20, 10), cs(20, 10), false ),
            ( cs(-20, -10), cs(-20, 10), false )
        );

    -- test data for "=" equality operator with different sized inputs
    constant td_equ_d : test_comp_data_84_type(0 to 15) :=
        (
            ( cs(0, 0), tcs4(0, 0), true),
            ( cs(0, 1), tcs4(0, 1), true),
            ( cs(1, 1), tcs4(1, 1), true),
            ( cs(-1, 1), tcs4(-1, 1), true),
            ( cs(-1, -1), tcs4(-1, -1), true),
            ( cs(-1, -6), tcs4(-1, -6), true),
            ( cs(-3, -6), tcs4(-3, -6), true),
            ( cs(-7, -7), tcs4(-7, -7), true),
            ( cs(-7, 7), tcs4(-7, 7), true),
            ( cs(7, 7), tcs4(7, 7), true),
            ( cs(-7, 7), tcs4(-7, 7), true),
            ( cs(-7, 7), tcs4(-7, -7), false),
            ( cs(-7, 7), tcs4(7, -7), false),
            ( cs(-7, 7), tcs4(7, 0), false),
            ( cs(-7, 0), tcs4(7, 0), false),
            ( cs(0, 7), tcs4(0, 0), false)
        );

    -- test helper functions/procedures
    type test_op is ('+', '-', '*', '/');

    procedure test_simple_operator(
        op : in test_op;        -- operator
        td : in test_data_type; -- test data to operate on
        pn : in boolean        -- positive or negative test?
    ) is
        variable left, right, expected, result : cs8;
        variable rep1, rep2, imp1, imp2 : s8;
    begin

        for i in td'range loop

            case op is
                when '+' =>
                    result := td(i).left + td(i).right;
                when '-' =>
                    result := td(i).left - td(i).right;
                when others =>
                    assert false report "Test for this operator not implemented!";
            end case;

            expected := td(i).expected;

            rep1 := get_real_part(expected);
            imp1 := get_img_part(expected);

            rep2 := get_real_part(result);
            imp2 := get_img_part(result);

            --print(result);
            --print(expected);

            assert ((rep1 = rep2) and (imp1 = imp2)) = pn
                report "Operator " & test_op'image(op) & " failed at index " & integer'image(i) & ".";
        end loop;
    end procedure;

begin  -- architecture test

    process is
        variable left, right, expected, result : cs8;
        variable rep1, rep2, imp1, imp2 : s8;
    begin  -- process

        -- test "+" operator
        test_simple_operator('+', td_add, true);
        test_simple_operator('+', td_add_n, false);

        -- test "-" subtraction operator
        test_simple_operator('-', td_sub, true);

        -- test multiplication "*" operator
        for i in td_mul'range loop
            assert (td_mul(i).left * td_mul(i).right) = td_mul(i).expected
                report "Multiplication operator failed at index" & integer'image(i) & ".";
        end loop;

        -- test multiplication operator for complex_signed * signed
        for i in td_mul_cs'range loop
            assert (td_mul_cs(i).left * td_mul_cs(i).right) = td_mul_cs(i).expected
                report "Multiplication of `complex_signed * signed` failed at index" & integer'image(i) & ".";
            -- test other way
            assert (td_mul_cs(i).right * td_mul_cs(i).left) = td_mul_cs(i).expected
                report "Multiplication of `signed * complex_signed` failed at index" & integer'image(i) & ".";
        end loop;

        -- test multiplication operator for complex_signed * integer
        for i in td_mul_ci'range loop
            assert (td_mul_ci(i).left * td_mul_ci(i).right) = td_mul_ci(i).expected
                report "Multiplication of `complex_signed * integer` failed at index " & integer'image(i) & ".";
            -- test other way
            assert (td_mul_ci(i).right * td_mul_ci(i).left) = td_mul_ci(i).expected
                report "Multiplication of `integer * complex_signed` failed at index " & integer'image(i) & ".";
        end loop;

        -- test division "/" operator
        for i in td_div_cs'range loop
            assert (td_div_cs(i).left / td_div_cs(i).right) = td_div_cs(i).expected
                report "Division of complex_signed to signed failed at index" & integer'image(i) & ".";
        end loop;

        for i in td_div_ci'range loop
            assert (td_div_ci(i).left / td_div_ci(i).right) = td_div_ci(i).expected
                report "Division of complex_signed to signed failed at index" & integer'image(i) & ".";
        end loop;

        -- test unary minus operator "-"
        for i in td_uminus'range loop
            assert ( - td_uminus(i).arg ) = td_uminus(i).expected
                report "Unary minus operator failed at index " & integer'image(i) & ".";
            -- also test the other way around
            assert ( - td_uminus(i).expected ) = td_uminus(i).arg
                report "Unary minus operator failed at index " & integer'image(i) & ".";
        end loop;

        -- test shift_right function
        for i in td_shift_right'range loop
            assert ( shift_right(td_shift_right(i).arg, td_shift_right(i).count) = td_shift_right(i).expected)
                report "Shifting right failed at index " & integer'image(i) & ".";
        end loop; -- td_shift_right

        -- test shift_left function
        for i in td_shift_left'range loop
            assert ( shift_left(td_shift_left(i).arg, td_shift_left(i).count) = td_shift_left(i).expected)
                report "Shiftin left failed at index " & integer'image(i) & ".";
        end loop; -- ts_shift_left

        -- test equality "=" operator
        for i in td_equ'range loop
            assert (td_equ(i).left = td_equ(i).right) = td_equ(i).expected
                report "Equality op failed at index " & integer'image(i) & ".";
        end loop;

        -- test "=" equality operator for different sized inputs
        for i in td_equ_d'range loop
            assert (td_equ_d(i).left = td_equ_d(i).right) = td_equ_d(i).expected
                report "Equality op failed at index " & integer'image(i) & ".";

            assert (td_equ_d(i).right = td_equ_d(i).left) = td_equ_d(i).expected
                report "Equality op failed at index " & integer'image(i) & ".";
        end loop;

        wait;

    end process;

    -- process for testing resize functions
    test_resize: process
        variable inp: complex_signed(7 downto 0);

        type test_resize_data_type is array(natural range<>) of natural;
        constant test_resize_data : test_resize_data_type(0 to 4) :=
            (11, 16, 32, 5, 3);

        variable out4 : complex_signed(3 downto 0);
    begin

        inp := to_cs(1, 1, inp'length);

        for i in test_resize_data'range loop
            assert resize(inp, test_resize_data(i))'left = test_resize_data(i)-1
                report "Resize failed: left index is wrong.";

            assert resize(inp, test_resize_data(i))'right = 0
                report "Resize failed: right index is wrong.";
        end loop;

        --
        inp := to_cs(50, 50, inp'length);
        out4 := resize(inp, 4);

        assert out4 = to_cs(2, 2, out4'length)
            report "Resize failed.";

        --
        inp := to_cs(-50, -50, inp'length);
        out4 := resize(inp, 4);
        assert out4 = to_cs(-2, -2, out4'length)
            report "Resize failed with negative number.";

        wait;

    end process test_resize;

    -- test conversion functions
    test_conversion: process
        variable outnum : complex_signed(7 downto 0);
        variable arg : cs8;
        variable cc : complex;
        variable cp : complex_polar;
    begin

        -- test different sized signed arguments
        assert false report "Below 2 warnings are expected." severity note;

        assert std_logic_vector(get_real_part(to_cs(to_signed(10, 8), to_signed(5, 4)))) = "00001010"
            report "Conversion failed, real part is wrong.";

        assert std_logic_vector(get_img_part(to_cs(to_signed(10, 8), to_signed(5, 4)))) = "00000101"
            report "Conversion failed, imaginary part is wrong.";

        -- test complex_signed to std_logic_vector conversion
        for i in td_slv'range loop
            assert to_std_logic_vector(td_slv(i).arg) = td_slv(i).expected
                report "Conversion to std_logic_vector failed at index " & integer'image(i) & ".";
        end loop;

        -- test std_logic_vector to complex_signed conversion
        for i in td_slv'range loop
            assert to_complex_signed(td_slv(i).expected) = td_slv(i).arg
                report "Conversion from std_logic_vector failed at index " & integer'image(i) & ".";
        end loop;

        -- test complex_polar to complex_signed
        cp := (1.0, 0.0);
        assert to_cs(cp, outnum'length) = to_cs(1, 0, outnum'length)
            report "Conversion from complex_polar failed.";

        cp := (1.0, MATH_PI/2.0);
        assert to_cs(cp, outnum'length) = to_cs(0, 1, outnum'length)
            report "Conversion from complex_polar failed.";

        cp := (10.0, MATH_PI/4.0);
        assert to_cs(cp, outnum'length) = to_cs(7, 7, outnum'length)
            report "Conversion from complex_polar failed.";

        -- test complex_signed to complex
        arg := cs(10, 10);
        cc := (10.0, 10.0);
        assert to_complex(arg) = cc
            report "Conversion to complex failed.";

        wait;

    end process test_conversion;

    -- test addition operator for different sized numbers
    test_diff_size_add:process
        variable num1 : complex_signed(7 downto 0);
        variable num2 : complex_signed(3 downto 0);
        variable result : complex_signed(7 downto 0);
    begin

        num1 := to_cs(10, 5, 8);
        num2 := to_cs(7, 5, 4);

        result := num1 + num2;

        assert result = to_cs(17, 10, 8)
            report "Addition of different sized numbers failed.";

        result := num2 + num1;

        assert result = to_cs(17, 10, 8)
            report "Addition of different sized numbers failed.";

        wait;

    end process test_diff_size_add;

end architecture test;
