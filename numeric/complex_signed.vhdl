-- An experimental complex number library with numeric_std integers
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use ieee.math_real.all;
use ieee.math_complex.all;

library std;
use std.textio.all;

--! A complex number package which is implemented with numeric_std.signed
--! type.
package complex_signed is

    --! base type of complex_signed vector. Only to be used internally.
    type complex_bit is record
        r : std_logic;
        i : std_logic;
    end record complex_bit;

    --! a vector type that represents a complex number with signed
    --! integer components
    type complex_signed is array (natural range <>) of complex_bit;

    -- CONVERSION FUNCTIONS

    --! Returns the real part of a complex_signed as a signed
    --! @param vec complex_signed input
    --! @return real part of `vec` as `signed(vec'length-1 downto 0)`
    function get_real_part (
        vec : complex_signed
        ) return signed;

    --! Returns the imaginary part of a complex_signed as a signed
    --! @param vec complex_signed input
    --! @return imaginary part of `vec` as `signed(vec'length-1 downto 0)`
    function get_img_part (
        vec : complex_signed
        ) return signed;

    --! Creates a complex_signed with real and imaginary parts given as
    --! signed type.
    --!
    --! Both inputs should be the same size. If it's not, smaller one
    --! will be resized to bigger one and a warning will be displayed.
    --!
    --! @param re signed real part
    --! @param im signed imaginary part
    --! @return `complex_signed(max(re'length, im'length)-1 downto 0)`
    function to_complex_signed (
        re : signed;
        im : signed
        ) return complex_signed;

    --! Creates a complex_signed with real and imaginary parts given as
    --! integers.
    --!
    --! Size of the vector should be provided as an extra argument.
    --!
    --! @param re integer real part
    --! @param im integer imaginary part
    --! @param size integer size of resulting vector
    --! @return `complex_signed(size-1 downto 0)`
    function to_complex_signed (
        re   : integer;
        im   : integer;
        size : positive
        ) return complex_signed;

    --! Converts `ieee.math_complex.complex` type to complex_signed.
    --! Input will be rounded with `integer()` method.
    --! @param c input to be converted to complex_signed
    --! @param size vector size of to returned value
    --! @return `complex_signed(size-1 downto 0)`
    function to_complex_signed (
        c    : complex;
        size : positive
        ) return complex_signed;

    --! Converts `ieee.math_complex.complex_polar` type to complex_signed.
    --! Input will be rounded with `integer()` method.
    --! @param c input to be converted to complex_signed
    --! @param size vector size of the returned value
    --! @return `complex_signed(size-1 downto 0)`
    function to_complex_signed (
        c    : complex_polar;
        size : positive
        ) return complex_signed;

    --! Converts `std_logic_vector` type to complex_signed.
    --! Upper half of the bits is used for real part, while lower
    --! half of the bits is used for imaginary part.
    --! @param vec input that will be converted to complex_signed, it should be
    --! in the form of `std_logic_vector(2*N-1 downto 0)`
    --! @return `complex_signed(N-1 downto 0)`
    function to_complex_signed (
        vec    : std_logic_vector
        ) return complex_signed;

    alias to_cs is to_complex_signed
        [signed, signed return complex_signed];

    alias to_cs is to_complex_signed
        [integer, integer, positive return complex_signed];

    alias to_cs is to_complex_signed
        [complex, positive return complex_signed];

    alias to_cs is to_complex_signed
        [complex_polar, positive return complex_signed];

    alias to_cs is to_complex_signed
        [std_logic_vector return complex_signed];

    --! Converts complex_signed to std_logic_vector.
    --! A complex signed vector converted to std_logic vector as follows:
    --! complex_signed(7 downto 0) -> std_logic_vector(15 downto 0)
    --! Upper bits of std_logic_vector (15 downto 8) carry the real part,
    --! lower bits (7 downto 0) carry the imaginary part.
    --! @param c
    --! @return std_logic_vector((c'length*2)-1 downto 0)
    function to_std_logic_vector(
        c : complex_signed
        ) return std_logic_vector;

    --! Converts complex_signed to ieee.math_complex.complex
    --! @param c input to be converted
    --! @return ieee.math_complex.complex
    function to_complex (
        c : complex_signed
        ) return complex;

    -- ARITHMETIC OPERATORS

    --! Addition operator for complex_signed, complex_signed
    --! @param l left side of the "+" operator
    --! @param r right side of the "+" operator
    --! @return result of l+r as `complex_signed(max(l'length, r'length)-1 downto 0)`
    function "+" (
        l : complex_signed;
        r : complex_signed
        ) return complex_signed;

    --! Subtraction operator for complex_signed, complex_signed
    --! @param l left side of the "-" operator
    --! @param r right side of the "-" operator
    --! @return result of l-r as `complex_signed(max(l'length, r'length)-1 downto 0)`
    function "-" (
        l : complex_signed;
        r : complex_signed
        ) return complex_signed;

    --! Multiplication operator for complex_signed, complex_signed
    --! @param l left side of the "*" operator
    --! @param r right side of the "*" operator
    --! @return result of l*r as `complex_signed((l'length + r'length)-1 downto 0)`
    function "*" (
        l : complex_signed;
        r : complex_signed
        ) return complex_signed;

    --! Multiplication operator for complex_signed, signed
    --! @param l left side of the "*" operator
    --! @param r right side of the "*" operator
    --! @return result of l*r as `complex_signed((l'length + r'length)-1 downto 0)`
    function "*" (
        l : complex_signed;
        r : signed
        ) return complex_signed;

    --! Multiplication operator for signed, complex_signed
    --! @param l left side of the "*" operator
    --! @param r right side of the "*" operator
    --! @return result of l*r as `complex_signed((l'length + r'length)-1 downto 0)`
    function "*" (
        l : signed;
        r : complex_signed
        ) return complex_signed;

    --! Multiplication operator for complex_signed, integer
    --! @param l left side of the "*" operator
    --! @param r right side of the "*" operator, integer will be assumed to be the size of left side
    --! @return result of l*r as `complex_signed(l'length*2-1 downto 0)`
    function "*" (
        l : complex_signed;
        r : integer
        ) return complex_signed;

    --! Multiplication operator for integer, complex_signed
    --! @param l left side of the "*" operator, integer will be assumed to be the size of left side
    --! @param r right side of the "*" operator
    --! @return result of l*r as `complex_signed(l'length*2-1 downto 0)`
    function "*" (
        l : integer;
        r : complex_signed
        ) return complex_signed;

    --! Division operator for complex_signed, signed
    --! @param l left side of the "/" operator
    --! @param r right side of the "/" operator
    --! @return result of l/r as `complex_signed(max(l'length, r'length)-1 downto 0)`
    function "/" (
        l : complex_signed;
        r : signed
        ) return complex_signed;

    --! Division operator for complex_signed, integer
    --! @param l left side of the "/" operator
    --! @param r right side of the "/" operator
    --! @return result of l/r as `complex_signed(l'length-1 downto 0)`
    function "/" (
        l : complex_signed;
        r : integer
        ) return complex_signed;

    --! Unuary minus operator
    --! @param c input value
    --! @return `-c`
    function "-"(
        c : complex_signed
        ) return complex_signed;

    -- COMPARISON OPERATORS

    --! Equality check operator for complex_signed, complex_signed.
    --! Before comparison smaller sized number is resized to bigger one.
    --! @param l left side of the "=" operator
    --! @param r right side of the "=" operator
    --! @return true if numbers are equal, false otherwise
    function "=" (
        l : complex_signed;
        r : complex_signed
        ) return boolean;

    -- SHIFT FUNCTIONS

    --! Shift complex_signed to right (reducing its value) by number of `counts`.
    --! Empty bits are filled with leftmost bit of the original input thus
    --! preserving to sign if the input.
    --! @param arg input value
    --! @param count number of bits to shift to right
    --! @return shifted input
    function shift_right(
        arg : complex_signed;
        count : natural
        ) return complex_signed;

    --! Shift complex_signed to left (increasing its value) by number of `counts`.
    --! Empty bits are filled with 0.
    --! @param arg input value
    --! @param count number of bits to shift to left
    --! @return shifted input
    function shift_left(
        arg : complex_signed;
        count : natural
        ) return complex_signed;

    -- RESIZE FUNCTIONS

    --! Resizes given complex_signed vector
    --! @param arg vector to resize
    --! @param new_size
    --! @return resized arg as `complex_signed(new_size-1 downto 0)`
    function resize(
        arg      : complex_signed;
        new_size : positive
        ) return complex_signed;

    --! Resizes given complex_signed vector to the size of another vector
    --! @param arg vector to resize
    --! @param sizeof
    --! @return resized arg as `complex_signed(sizeof'length-1 downto 0)`
    function resize(
        arg      : complex_signed;
        sizeof   : complex_signed
        ) return complex_signed;

    -- HELPER FUNCTIONS AND PROCEDURES

    --! Prints the given complex_signed in the form of (r, i).
    procedure print(val : in complex_signed);

end package;

package body complex_signed is

    -- NON PUBLIC FUNCTIONS
    function max(left, right : integer) return integer is
    begin
        if left > right then
            return left;
        else
            return right;
        end if;
    end function max;

    -- PUBLIC IMPLEMENTATIONS
    function get_real_part (     -- returns the real part of a vector
        vec : complex_signed)
        return signed is
    variable val : signed(vec'RANGE);
    begin

        for i in vec'low to vec'high loop
            val(i) := vec(i).r;
        end loop;  -- i

        return val;
    end function;

    function get_img_part (     -- returns the imaginary part of a vector
        vec : complex_signed)
        return signed is
    variable val : signed(vec'RANGE);
    begin

        for i in vec'low to vec'high loop
            val(i) := vec(i).i;
        end loop;  -- i

        return val;
    end function;

    -- Conversion functions
    function to_complex_signed ( -- from numeric_std.signed
        re : signed;
        im : signed)
    return complex_signed is
        constant size : positive := max(re'length, im'length);
        variable val : complex_signed(size-1 downto 0);
        variable sre, sim : signed(size-1 downto 0); -- resized inputs
    begin

        sre := resize(re, size);
        sim := resize(im, size);

        assert re'LENGTH = im'LENGTH
            report "Real and imaginary parts are not the same size, using the bigger size"
            severity WARNING;

        for i in val'range loop
            val(i).r := sre(i);
            val(i).i := sim(i);
        end loop;  -- i

        return val;

    end function to_complex_signed;

    function to_complex_signed (  -- from integers
        re   : integer;
        im   : integer;
        size : positive)
        return complex_signed is
    variable val : complex_signed(size-1 downto 0);
    begin

        val := to_complex_signed(to_signed(re, size),
                                 to_signed(im, size));
        return val;

    end function to_complex_signed;

    -- converts ieee.math_complex.complex type to complex_signed.
    -- Input will be rounded with integer() function
    function to_complex_signed (
        c    : complex;
        size : positive
    ) return complex_signed is
        variable val : complex_signed(size-1 downto 0);
    begin

        val := to_complex_signed(integer(c.re), integer(c.im), size);
        return val;

    end function to_complex_signed;

    function to_complex_signed (
        c    : complex_polar;
        size : positive
    ) return complex_signed is
    begin
        return to_complex_signed(polar_to_complex(c), size);
    end function;

    function to_complex_signed (
        vec    : std_logic_vector
    ) return complex_signed is
    begin
        assert (vec'length) rem 2 = 0
            report "std_logic_vector length must be an even number for conversion to complex_signed."
            severity FAILURE;

        return to_complex_signed(signed(vec(vec'high downto vec'length/2)),
                                 signed(vec(vec'length/2-1 downto 0)));
    end function to_complex_signed;

    function to_std_logic_vector(
        c : complex_signed
    ) return std_logic_vector is
        variable svec : std_logic_vector((c'length*2)-1 downto 0);
    begin
        svec(svec'high downto (svec'length/2)) := std_logic_vector(get_real_part(c));
        svec((svec'length/2)-1 downto 0) := std_logic_vector(get_img_part(c));
        return svec;
    end function to_std_logic_vector;

    function to_complex (
        c : complex_signed
    ) return complex is
    begin
        return cmplx(real(to_integer(get_real_part(c))),
                       real(to_integer(get_img_part(c))));
    end function to_complex;

    -- addition operator for complex_signed type
    function "+" (
        l : complex_signed;
        r : complex_signed
    ) return complex_signed is
        constant size : positive := max(l'length, r'length);
        variable l_re, l_im, r_re, r_im : signed(size-1 downto 0);
    begin

        l_re := get_real_part(resize(l, size));
        l_im := get_img_part(resize(l, size));
        r_re := get_real_part(resize(r, size));
        r_im := get_img_part(resize(r, size));

        return to_cs(l_re+r_re, l_im+r_im);

    end function;

    -- subtraction operator for complex_signed type
    function "-" (
        l : complex_signed;
        r : complex_signed
    ) return complex_signed is
        constant size : positive := max(l'length, r'length);
        variable l_re, l_im, r_re, r_im : signed(size-1 downto 0);
    begin

        l_re := get_real_part(resize(l, size));
        l_im := get_img_part(resize(l, size));
        r_re := get_real_part(resize(r, size));
        r_im := get_img_part(resize(r, size));

        return to_cs(l_re - r_re, l_im - r_im);

    end function;

    function "*" (
        l : complex_signed;
        r : complex_signed
    ) return complex_signed is
        constant size : positive := max(l'length, r'length);
        constant rsize : positive := l'length + r'length + 1;

        variable l_re, l_im : signed(size-1 downto 0);
        variable r_re, r_im : signed(size-1 downto 0);

        variable result_re, result_im : signed(rsize-1 downto 0);
    begin
        l_re := get_real_part(resize(l, size));
        l_im := get_img_part(resize(l, size));
        r_re := get_real_part(resize(r, size));
        r_im := get_img_part(resize(r, size));

        result_re := resize(l_re * r_re, rsize) - resize(l_im * r_im, rsize);
        result_im := resize(l_im * r_re, rsize) + resize(l_re * r_im, rsize);

        return to_cs(result_re, result_im);
    end function;

    function "*" (
        l : complex_signed;
        r : signed
    ) return complex_signed is
        constant size : positive := max(l'length, r'length);
        constant rsize : positive := l'length + r'length;

        variable l_re, l_im : signed(size-1 downto 0);

        variable result_re, result_im : signed(rsize-1 downto 0);
    begin

        l_re := get_real_part(resize(l, size));
        l_im := get_img_part(resize(l, size));

        result_re := l_re * r;
        result_im := l_im * r;

        return to_cs(result_re, result_im);

    end function;

    function "*" (
        l : signed;
        r : complex_signed
    ) return complex_signed is
    begin
        return r * l;
    end function;

    function "*" (
        l : complex_signed;
        r : integer
    ) return complex_signed is
    begin
        return l * to_signed(r, l'length);
    end function;

    function "*" (
        l : integer;
        r : complex_signed
    ) return complex_signed is
    begin
        return r * l;
    end function;

    function "/" (
        l : complex_signed;
        r : signed
    ) return complex_signed is
        constant size : positive := max(l'length, r'length);
        variable l_re, l_im, r_re : signed(size-1 downto 0);
    begin

        l_re := get_real_part(resize(l, size));
        l_im := get_img_part(resize(l, size));
        r_re := resize(r, size);

        return to_cs(l_re / r_re, l_im / r_re);

    end function;

    function "/" (
        l : complex_signed;
        r : integer
    ) return complex_signed is
    begin
        return l / to_signed(r, l'length);
    end function;

    function "-"(
        c : complex_signed
    ) return complex_signed is
    begin
        return to_cs( - get_real_part(c), - get_img_part(c));
    end function;

    function "=" (
        l : complex_signed;
        r : complex_signed
    ) return boolean is
        constant size : positive := max(l'length, r'length);
        variable l_re, l_im, r_re, r_im : signed(size-1 downto 0);
    begin
        l_re := get_real_part(resize(l, size));
        l_im := get_img_part(resize(l, size));
        r_re := get_real_part(resize(r, size));
        r_im := get_img_part(resize(r, size));

        return (l_re = r_re) and (l_im = r_im);
    end function;

    -- implementation of shift functions
    function shift_right(
        arg : complex_signed;
        count : natural
    ) return complex_signed is
        constant size : positive := arg'length;
        variable re, im : signed(size-1 downto 0);
        variable r : complex_signed(arg'range);
    begin
        r(r'high-count downto 0) := arg(arg'high downto count);
        for i in r'high downto r'high-count+1 loop
            r(i) := arg(arg'high);
        end loop;
        return r;
    end function shift_right;

    function shift_left(
        arg : complex_signed;
        count : natural
    ) return complex_signed is
        variable r : complex_signed(arg'range);
    begin
        r(r'high downto count) := arg(arg'high-count downto 0);
        for i in count-1 downto 0 loop
            r(i) := ('0', '0');
        end loop;
        return r;
    end function shift_left;

    -- implementation of resize functions
    function resize(
        arg      : complex_signed;
        new_size : positive
    ) return complex_signed is
        variable rep, imp : signed(new_size-1 downto 0);
    begin
        -- do not bother if already the desired
        if new_size /= arg'length then
            rep := resize(get_real_part(arg), new_size);
            imp := resize(get_img_part(arg), new_size);
            return to_cs(rep, imp);
        else
            return arg;
        end if;
    end function resize;

    function resize(
        arg      : complex_signed;
        sizeof   : complex_signed
    ) return complex_signed is
        constant new_size : positive := sizeof'length;
    begin
        return resize(arg, new_size);
    end function resize;

    procedure print(val : in complex_signed) is
        variable re, im : integer;
        variable l : line;
    begin
        re := to_integer(get_real_part(val));
        im := to_integer(get_img_part(val));

        write(l, "(" & integer'image(re) & ", " & integer'image(im) & ")");
        writeline(output, l);
    end procedure print;

end package body complex_signed;
