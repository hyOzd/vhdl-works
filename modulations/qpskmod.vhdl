--
--	QPSK-Modulation module by Hasan Yavuz �ZDERYA ( hy [at] ozderya.net )
--	
--	Trabzon, KTU DSPLAB 17.05.2013
--	
--	Desc: numeric types are chosen for compatibility with cordicB module
--	and mapping is;
--	01-00		s1-s0
--	11-10		s2-s3

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity qpskmod is
	
	Generic( n:integer := 16 );
	Port( symbol	: in 	std_logic_vector(1 downto 0);
		  mod_r		: out	signed(n-1 downto 0);
		  mod_c		: out	signed(n-1 downto 0);
		  clk		: in 	std_logic;
		  reset		: in	std_logic
		 );
	
	subtype tfixed is signed(n-1 downto 0);
		 
end entity qpskmod;

architecture behav of qpskmod is

	-- scalar for moving fixed numbers to integers
	constant fx_c1		: real := 2**(real(n)-2.0);	-- for Q1.14
	
	constant the_one: tfixed := to_signed(integer(MATH_1_OVER_SQRT_2*fx_c1),n);
	constant the_mone: tfixed := to_signed(integer(-MATH_1_OVER_SQRT_2*fx_c1),n);
	constant the_zero: tfixed := to_signed(0,n);
	
begin
	
	process(clk,reset)
	begin
		
		if reset = '1' then
			mod_r <= the_zero;
			mod_c <= the_zero;
		
		elsif rising_edge(clk) then
			
			case symbol is
				
				when "00" =>
					mod_r <= the_one;
					mod_c <= the_one;
				
				when "01" =>
					mod_r <= the_one;
					mod_c <= the_mone;
					
				when "10" =>
					mod_r <= the_mone;
					mod_c <= the_one;
					
				when "11" =>
					mod_r <= the_mone;
					mod_c <= the_mone;
					
				when others =>
					mod_r <= (others => 'X');
					mod_c <= (others => 'X');
				
			end case;
			
		end if;
		
	end process;
	
end architecture behav;