library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity tb_mod_demo_2 is
end entity;

architecture behav of tb_mod_demo_2 is
	
	signal clk:std_logic;
	signal reset:std_logic;
	signal stop_clock:boolean := false;
	constant CLK_PERIOD:time := 10 ns;
	
	constant n :integer := 16;
	
	-- scalars for moving fixed numbers to integers
	constant fx_c1		: real := 2**(real(n)-2.0);	-- for Q1.14
	constant fx_c2		: real := 2**(real(n)-4.0);  -- for Q3.12
	
	signal modulated : signed(n-1 downto 0);
	signal r_modulated: real;
	signal phaseinc : signed(n-1 downto 0);
	
	constant phaseinc_1 : signed(n-1 downto 0) := to_signed(integer(2.0*MATH_PI* fx_c2 / real(1000/20) ),n);
	constant phaseinc_2 : signed(n-1 downto 0) := to_signed(integer(2.0*MATH_PI* fx_c2 / real(1000/40) ),n);
	
begin

	r_modulated <= real(to_integer(modulated))/fx_c1;	
	
	uut:entity work.mod_demo_2
		generic map(1000,
					20,
					20,
					n)
		port map(
			phaseinc,
			modulated,
			clk,
			reset
		);

	clkproc:process
	begin
		if (not stop_clock) then
			clk <= '1';
			wait for CLK_PERIOD/2;
			clk <= '0';
			wait for CLK_PERIOD/2;
		else
			wait;
		end if;
	end process clkproc;
	
	stimproc:process
	begin
		reset <= '1';
		wait for CLK_PERIOD;
		reset <= '0';
		
		phaseinc <= phaseinc_1;
		
		wait for CLK_PERIOD*500;
		
		phaseinc <= phaseinc_2;
		
		wait for CLK_PERIOD*500;
	
		stop_clock <= true;
		wait;
	end process stimproc;

end behav;