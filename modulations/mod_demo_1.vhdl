--
--	Modulation Demo 1
--
--	by Hasan Yavuz �ZDERYA ( hy [at] ozderya.net )
--	
--	Trabzon KTU DSPLAB 17.05.2013
--
--	This module is a demo of below chain
--
--	 ___________	  _________		 ____________	   ___________
--	|			|	 |		   |	|			 |	  |			  |
--	|	LFSR	|--->|	SIPO   |--->|  QPSK-mod	 |--->|	  IQ-mod  |-->(modulated)
--	|___________|	 |_________|	|____________|	  |___________|
--
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity mod_demo_1 is
	
	Generic(freq_in : integer := 100000;	-- kHz
			freq_crr: integer := 1000;		-- desired Carrier Freq.
			bit_rate: integer := 1000;		-- bit clock freq.	
			n		: integer := 16;
			seed	: integer := 1			-- bit seq. seed
			);
			
	Port( modulated		: out signed(n-1 downto 0);
		  clk			: in std_logic;
		  reset			: in std_logic
		 );
	
	subtype tfixed is signed(n-1 downto 0);	 
		
end entity mod_demo_1;

architecture rtl of mod_demo_1 is

	-- scalars for moving fixed numbers to integers
	constant fx_c1		: real := 2**(real(n)-2.0);	-- for Q1.14
	constant fx_c2		: real := 2**(real(n)-4.0);  -- for Q3.12
	
	constant phaseinc : tfixed := to_signed(integer(2.0*MATH_PI* fx_c2 / real(freq_in/freq_crr) ),n);
	
	signal mod_r,mod_c,crd_modulated : tfixed;
	
	signal ser_bits : std_logic;
	signal par_bits : std_logic_vector(1 downto 0);
	signal clkb,clks: std_logic := '0';	-- bit clock
	
begin
	
	modulated <= crd_modulated;
	
	uiqm:entity work.cordicB_iqmod
		generic map(n,16)
		port map(
			phaseinc,
			mod_r,mod_c,
			crd_modulated,
			clk,
			reset	
		);
	
	-- random bit seq. generator
	urbg:entity work.lfsr
		generic map(16,45)
		port map(
			open,
			ser_bits,
			clkb,
			reset
		);
	
	-- serial to parallel
	usipor:entity work.sipor
		generic map(2)
		port map(
			ser_bits,
			par_bits,
			clks,
			clkb,
			reset
		);
	
	-- qpsk modulator
	uqpsk:entity work.qpskmod
		generic map(n)
		port map(
			par_bits,
			mod_r,mod_c,
			clk,
			reset
		);
	
	bitclock:process(clk,reset)
		constant div :integer := freq_in/bit_rate;
		variable count : integer range 0 to div/2 := 0; 
	begin
		if reset = '1' then
			count := 0;
			clkb <= '0';
		elsif rising_edge(clk) then
			count := count+1;
			if count = div/2 then
				count := 0;
				clkb <= clkb xor '1';
			end if;
		end if;
	end process;
	
end architecture;