library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity tb_mod_demo_1 is
end entity;

architecture behav of tb_mod_demo_1 is
	
	signal clk:std_logic;
	signal reset:std_logic;
	signal stop_clock:boolean := false;
	constant CLK_PERIOD:time := 10 ns;
	
	constant n :integer := 16;
	
	-- scalars for moving fixed numbers to integers
	constant fx_c1		: real := 2**(real(n)-2.0);	-- for Q1.14
	
	signal modulated : signed(n-1 downto 0);
	signal r_modulated: real;
	
begin

	r_modulated <= real(to_integer(modulated))/fx_c1;	
	
	uut:entity work.mod_demo_1
		generic map(1000,
					20,
					20,
					n)
		port map(
			modulated,
			clk,
			reset
		);

	clkproc:process
	begin
		if (not stop_clock) then
			clk <= '1';
			wait for CLK_PERIOD/2;
			clk <= '0';
			wait for CLK_PERIOD/2;
		else
			wait;
		end if;
	end process clkproc;
	
	stimproc:process
	begin
		reset <= '1';
		wait for CLK_PERIOD;
		reset <= '0';
		
		wait for CLK_PERIOD*1000;
	
		stop_clock <= true;
		wait;
	end process stimproc;

end behav;