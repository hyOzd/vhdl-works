library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity tb_qpskmod is
end entity;

architecture behav of tb_qpskmod is
	
	signal clk:std_logic;
	signal reset:std_logic;
	signal stop_clock:boolean := false;
	constant CLK_PERIOD:time := 10 ns;
	
	constant n:integer := 16;
	
	subtype tfixed is signed(n-1 downto 0);
	
	-- scalar for moving fixed numbers to integers
	constant fx_c1		: real := 2**(real(n)-2.0);	-- for Q1.14
	
	type arr is array(natural range<>) of std_logic_vector(1 downto 0);
	constant all_symbols : arr (0 to 3) := ("00","01","10","11");
		
	signal symbols:std_logic_vector(1 downto 0) := "00";
	signal mod_r,mod_c : tfixed;
	
begin

	uut:entity work.qpskmod
		generic map(n)
		port map( 
			symbols,
			mod_r,mod_c,
			clk,
			reset
		);

	clkproc:process
	begin
		if (not stop_clock) then
			clk <= '0';
			wait for CLK_PERIOD/2;
			clk <= '1';
			wait for CLK_PERIOD/2;
		else
			wait;
		end if;
	end process clkproc;
	
	stimproc:process
	begin
		
		reset <= '1';
		wait for CLK_PERIOD;
		reset <= '0';
		
		for i in all_symbols'RANGE loop
			
			symbols <= all_symbols(i);
			wait for CLK_PERIOD;
			
		end loop;
	
		stop_clock <= true;
		wait;
	end process stimproc;

end behav;